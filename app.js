const rootRouter =require('./routes/index')
const express = require("express")
const app = express()
const cookieparser = require("cookie-parser");
require('./db/mongoose');

app.use(cookieparser())
app.use(express.json());
app.use(express.urlencoded({ extended: false }));app.set('view engine',"ejs")
app.use(express.json())
app.get("/",(req,res)=>{
    res.send("Welcome to Mongodb Assignment")
})
app.use(rootRouter)

app.listen(process.env.PORT,()=>{
    console.log("Server is rinning on port :",process.env.PORT)
})