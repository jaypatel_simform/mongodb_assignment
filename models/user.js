const mongoose = require("mongoose")
const validator = require("validator")

const userSchema = new mongoose.Schema({
    id:{
        type:String
    },

    first_name:{
        type:String,
    },
    last_name:{
        type:String,
    },
    email:{
        type:String,
        unique:true,
        trim:true,
        lowercase:true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Invalid Email..')
            }
        }
    },
    gender:{
        type:String,
         
    },
    age:{
        type:Number
    },
    bdate:{
        type:String
    },
    currency:{
        type:String
    },
    car:{
        type:String
    },
    domain:{
        type:String
    },
    wallet:{
        type:Number
    }
},{
    timestamps:true
})

userSchema.post('save',async function (){
    console.log("#############..User saved..#############\n",this)
})
const User = mongoose.model("User",userSchema)
module.exports = User