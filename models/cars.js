const mongoose = require("mongoose")
const validator = require("validator")

const carSchema = new mongoose.Schema({
    cName:{type:String},
    model:{type:String},
    year:{type:Number},
    weight:{type:Number},
    engine:{type:Number},
    fule:{
        type:String,
        validate(v){
            if(!["petrol","diesel","gas"].includes(v))
            throw new Error("enter valid fule type")
        }
    },
    price:{type:Number,default:10000},
    colours:{}

},{
    timestamps:true
})

carSchema.post('save',async function (){
    console.log("#############..car saved..#############\n",this)
})
const Car = mongoose.model("Car",carSchema)
module.exports = Car