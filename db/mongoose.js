const mongoose = require("mongoose")
mongoose.connect(process.env.MONGOURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    
}).then(() => {
    console.log("mongodb connected successfully")
}).catch((err) => { 
    console.log("error while connceting to database",err)
})    