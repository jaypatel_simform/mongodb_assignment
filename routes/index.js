const users = require('./user')
const agg = require('./aggregation')
const car = require("./cars")
const express = require("express")
const rootRouter = new express.Router()

rootRouter.use('/users', users);
rootRouter.use('/agg', agg);
rootRouter.use('/car', car);

module.exports = rootRouter