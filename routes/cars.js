const express = require('express')
const router = new express.Router()
const Car = require('../models/cars')

router.post('/', async (req, res) => {
    try {
        const car = new Car(req.body)
        await car.save()
        res.send(car)
    } catch (err) {
        res.send({ errro: err.message })
    }
})

router.get('/', async (req, res) => {
    try {
        const cars = await Car.find({})
        res.send(cars)
    } catch (err) {
        res.send({ errro: err.message })
    }
})

// GET avg price of cars by year/fule/colours
//use unwind to get the result
router.get('/avgprice', async (req, res) => {
    try {
        let by = req.query.by ? req.query.by : ""
        const cars = await Car.aggregate([
            // { $match: { colours: { $in: ["red"] } } },
            { $unwind: { path: "$"+by, preserveNullAndEmptyArrays: false } },
            { $group: { _id: "$"+by, averagePrice: { $avg: "$price" } } },
            { $sort: { "averagePrice": -1 } }])
        res.send(cars)
    } catch (err) {
        res.send({ errro: err.message })
    }
})

router.patch('/:id', async (req, res) => {
    try {
        const car = Car.findByIdAndUpdate(req.params.id, { price: req.body.price }, { new: true, runValidators: true })
        res.send(car)
    } catch (err) {
        res.send({ errro: err.message })
    }
})
module.exports = router