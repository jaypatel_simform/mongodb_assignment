const { render } = require('ejs')
const express = require('express')
const router = new express.Router()
const User = require('../models/user')
const Car = require("../models/cars")


// Get users details using "GroupBy"
// group using key value : { car } , { last_name } , { age } 
// also you can use sorting feature
// output ; groupBy { car : totalCount } - car model used by total number of users  
router.get('/gp/user', async (req, res) => {
    let by, data, length, error;
    //aggregation by value like gender 
    if (req.query.gpBy) {
        by = req.query.gpBy
        fb = req.query.filterBy
        order = req.query.order ? req.query.order : 1
        filterBy = req.query.filterBy ? { [fb]: req.query.value } : {}
        value = req.query.value ? req.query.value : ""

        //data = await User.distinct(by)
        console.log("filby", filterBy)
        data = await User.aggregate([
            { $match: filterBy },
            { $group: { _id: "$" + by, count: { $sum: 1 } } },
            { $sort: { count: Number(order) } }
        ])
        length = data.length
        console.log("data1", by, data)
        error = 0
    }
    else {
        error = 1
    }
    res.send({ GroupBy: by, FilterBy: { filterBy, value }, length, data })
})

// Get cars details using "GroupBy"
// group using key value : { model } , { year } , { fule } 
// also you can use sorting feature
router.get('/gp/car', async (req, res) => {
    let by, data, length, error;
    if (req.query.gpBy) {
        by = req.query.gpBy
        fb = req.query.filterBy
        order = req.query.order ? req.query.order : 1
        filterBy = req.query.filterBy ? { [fb]: req.query.value } : {}
        value = req.query.value ? req.query.value : ""

        console.log("filby", filterBy)
        data = await Car.aggregate([
            { $match: {} },
            { $group: { _id: "$" + by, count: { $sum: 1 } } },
            { $sort: { count: Number(order) } }
        ])
        length = data.length
        console.log("data1", by, data)
        error = 0
    }
    else {
        error = 1
    }
    res.send({ GroupBy: by, FilterBy: { filterBy, value }, length, data })
})

// GET users data using project filter
router.get('/prj', async (req, res) => {
    try {
        let p = { _id: 0 }
        if (req.query.prjBy) {

            req.query.prjBy.split(' ').forEach(e => {
                Object.assign(p, { [e]: 1 })
            });
        }
        const user = await User.aggregate([
            { $match: {} },
            { $project: p }
        ])
        res.send(user)
    } catch (err) {
        res.send(err.message)
    }
})

// Get car name with it's owner details
router.get('/lookup', async (req, res) => {

    const data = await Car.aggregate([
        { "$match": {} },
        {
            "$lookup": {
                "from": "users",
                "let": { "car": "$cName" },
                "pipeline": [
                    {   
                        "$match": {
                            "$expr": { "$eq": ["$car", "$$car"] },
                        }
                    },
                    { $project: { _id: 0, createdAt: 0, updatedAt: 0, domain: 0, car: 0 } }
                ],
                "as": "Car Owner"
            }
        },
        { $project: { _id: 0, createdAt: 0, updatedAt: 0 } }
    ])
    res.send(data)
})
module.exports = router   