const express = require('express')
const mongoose = require('mongoose')
const { find, findById } = require('../models/user')
const router = new express.Router()
const User = require('../models/user')

// Get users detais with filter and project , sort features
router.get('/', async (req, res) => {
    try {
        let count, by, user
            let p = { _id: 0 } // variable for projection
            // set value for projection query
            if (req.query.prjBy) {
                req.query.prjBy.split(' ').forEach(e => {
                    Object.assign(p, { [e]: 1 })
                });
            }
            console.log(p)
        // set limit and skip variable for pagination 
        const limit = req.query.limit ? req.query.limit : 1000
        const skip = req.query.skip ? req.query.skip : 0
        // also can use sortby feature : "age:-1"

        const sort = req.query.sortBy ? { [req.query.sortBy]: Number(req.query.order) } : {}
        // find user by : "car = alto", "age:20" use key:pair to find users
        if (req.query.searchBy) {
            console.log("in seachBy query")
            by = req.query.searchBy
            user = await User.aggregate([
                { $match: { [by]: req.query.value } },
                { $sort: sort },
                { $project: p }
            ])
            count = user.length
            return res.json({ count, user })
        }
        else {
            user = await User.find().sort({ _id: -1 })
            return res.send({ count: user.length, user })//.limit(limit).skip(skip)
        }
    } catch (err) {
        res.send(err.message)
    }
})

// Get users details by using regex
// you can find something in value : { email : "uk" } , find uk in each email
router.get('/regex', async (req, res) => {
    try {
        let count, by, user
        const limit = req.query.limit ? req.query.limit : 50
        const skip = req.query.skip ? req.query.skip : 0
        const sort = req.query.sortBy ? { [req.query.sortBy]: Number(req.query.order) } : {}
        if (req.query.searchBy) {
            console.log("in seachBy query")
            by = req.query.searchBy
            user = await User.aggregate([
                { $match: { [by]: { $regex: req.query.value } } },
                { $project: { _id: 0 } },
                { $sort: sort }
            ])
            count = user.length
            return res.send({ count, user })
        }
        else {
            user = await User.find().sort({ _id: -1 })
            return res.send({ count: user.length, user }).limit(limit).skip(skip)
        }
    } catch (err) {
        res.send({ error: err.message })
    }
})

router.post('/', async (req, res) => {
    try {
        const user = new User(req.body)
        console.log("user", user)
        await user.save()
        res.send(`${user.first_name} is saved`)
    } catch (err) {
        res.send(err.message)
    }
})
router.get('/profile/:id', async (req, res) => {
    try {
        const id = req.params.id
        const user = await User.findById(id)
        if (!user)
            res.send({ error: "User not found" })
        const keys = Object.keys(user)
        //res.render('profile',{user,keys})
        res.send(user)
        //res.send(user)
    } catch (err) {
        res.send({ error: err.message })
    }
})

router.patch('/profile/:id', async (req, res) => {
    console.log("in patch")
    try {
        const id = req.params.id
        const user = await User.findByIdAndUpdate(id, req.body, { new: true })
        if (!user)
            res.send({ error: "User not found" })
        console.log("updated user", user)

        res.send(user)
    } catch (err) {
        res.send({ error: err.message })
    }
})

router.delete('/:id', async (req, res) => {
    console.log("in delete")
    try {
        const id = req.params.id
        const user = await User.findByIdAndDelete(id)
        if (!user)
            return res.send("user not found")
        console.log("deleted user")
        //res.render('home',{user})
        //res.redirect('/')
        res.send(`${user.first_name} ${user.last_name} has been deleted..!`)
    } catch (err) {
        res.send({ error: err.message })
    }
})

router.get("/updateDB", async (req, res) => {
    try {
        const users = await User.find({})
        for (let u of users) {
            u.wallet = 10000
            await u.save()
        }
        res.send("ok")
    } catch (err) {

    }
})
router.post("/giveRewardsAll/:amount", async (req, res) => {
    try {
        const amount = req.params.amount
        const session = await mongoose.startSession()
        //;(await session).withTransaction
        await session.withTransaction(async () => {
            await User.bulkWrite([{
                updateMany: {
                    "filter": {},
                    "update": { $inc: { "wallet": amount } }
                }
            }])
        })
        res.send(`Give rewards to all worth of ${amount}`)
    } catch (er) {
        res.json({ er: er.message })
    }
})

router.post("/transaction", async (req, res) => {
    try {
        const sender = req.body.sender
        const receiver = req.body.receiver
        const amount = req.body.amount

        const session = await mongoose.startSession()
        session.startTransaction()

        try {
            const sender = await User.findById(req.body.sender).session(session)
            if (sender.wallet < amount)
                throw new Error(`User ${sender.first_name} you have insufficient balance`)
            else
                sender.wallet = sender.wallet - amount;

            const receiver = await User.findById(req.body.receiver).session(session)
            receiver.wallet = receiver.wallet + amount

            await sender.save()
            await receiver.save()
            await session.commitTransaction()

            return res.json({ sender: sender.first_name, receiver: receiver.first_name, amount: amount, yourBalance: sender.wallet })

        } catch (err) {
            console.log("in abort :", err.message)
            await session.abortTransaction()
        } finally {
            session.endSession()
        }
    } catch (err) {
        console.log("err", err.message)
        return res.send(`Transaction falied`)
    }
})

module.exports = router