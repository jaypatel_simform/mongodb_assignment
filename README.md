- created APIs to manipulate data of users and cars
- create APIs for find, fetch, patch
- used Grouping, Projection, Pipeline, Lookup, Pagination
- Create transaction feature to send wallet amount to another users account


- Also created docker container for the app.
	-created 2 containers : 
	-	1) Container for API
	-	2) Container for Mongodb